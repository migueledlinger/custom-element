## Getting started 

1. reopen project in devcontainer 
2. run `npx webpack` 
3. run `npx http-server .`
4. open `http://localhost:8080/` in your browser

### Side Notes
JSON format to write in 'DATA' textfield: `{ "text": "YES OR NO QUESTION"}` 