import CheckBox from "devextreme/ui/check_box";
import RadioGroup from "devextreme/ui/radio_group"

export class YesNoItem extends HTMLElement {
    static get observedAttributes() { return ['data', 'state'] }
    

    dxRadioGroup;

    constructor() {
        super();

        this.attachShadow({ mode: "open" });

        const wrapper = document.createElement("div");
        wrapper.id = "wrapper";
        wrapper.style.backgroundColor = "yellow";
        wrapper.style.padding = "20px"

        wrapper.insertAdjacentHTML( "afterbegin", `
        <p id="questionParagraph"></p>
        <div id="dxYesNo" style="
            border-style: groove;
            border-radius: 6px;
            padding: 10px;">
        </div>
        `);

        wrapper.title = "yes-no-item";

        const dxStyles = document.createElement('link');
        dxStyles.setAttribute('rel', 'stylesheet');
        dxStyles.setAttribute('href', 'node_modules/devextreme/dist/css/dx.light.css');

        this.shadowRoot.appendChild(dxStyles);
        this.shadowRoot.append(wrapper);
     
        const properties = ['yes', 'no'];
        const options = {items: properties};

        const dxYesNO = this.shadowRoot.getElementById("dxYesNo");

        this.dxRadioGroup = new RadioGroup(dxYesNO, options);


        this.dxRadioGroup.on("valueChanged", (e) => this.changeStateDx(e));

        const questionPara = this.shadowRoot.getElementById("questionParagraph");
        try {

            const data =  JSON.parse(this.getAttribute("data"));
            
            questionPara.innerText = data.text;
            
        } catch(e) {
            
            questionPara.innerText = "";
        }
    }

   // 1) fires when yes or no radio button is selected
    changeStateDx(element) {
        const item = this;
        let new_state = element.value;

        new_state = `{"selected": "${new_state}"}`;

        item.setAttribute("state", new_state);

        // 2) if yes or no btn was clicked fire event "stateHasChanged"
        if (document.getElementById("item").dispatchEvent(new CustomEvent("stateChanged"))) {
        }
    }

    // extracts question text from json
    attributeChangedCallback(name, oldValue, newValue) {

        
        if (name == "data")
        {
            try {
                const text = JSON.parse(newValue).text;

                if (text)
                {
                    this.shadowRoot.getElementById("questionParagraph").innerText = text;
                } 
                else 
                {
                    this.shadowRoot.getElementById("questionParagraph").innerText = "";
                }
                
            } catch {
                this.shadowRoot.getElementById("questionParagraph").innerText = "";
            }
        }
        if (name == "state")
        {
            try {
                const state = JSON.parse(newValue).selected;
                if (state == "no" || state == "yes")
                {
                    this.dxRadioGroup.option('value', state);
                }    
            } catch {

            }
        }
          
      }
}