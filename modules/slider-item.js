import Slider from "devextreme/ui/slider"

export class SliderItem extends HTMLElement 
{
    constructor()
    {
        super();

        this.attachShadow({mode: "open"});

        const wrapper = document.createElement("div");
        wrapper.style = `text-align: center; 
                         align-items: center;
                         border-style: solid;
                         padding: 5px;`;

        wrapper.title = "slider-item";
        const sliderDiv = document.createElement("div");
        wrapper.append(sliderDiv);

        const dxSlider = new Slider(sliderDiv, {value: 50, max: 100, min: 0});

        dxSlider.on("valueChanged", (slider) => this.displaySlideVal(slider));

        const dxStyles = document.createElement('link');
        dxStyles.setAttribute('rel', 'stylesheet');
        dxStyles.setAttribute('href', 'node_modules/devextreme/dist/css/dx.greenmist.css');

        const valDisplay = document.createElement("p");
        valDisplay.id = "slideValDisplay";
        valDisplay.style = `border-style: solid;
                            padding: 2px;
                            width: 10%;
                            margin-left: auto;
                            margin-right: auto;
                            text-align: center;`;

        valDisplay.innerText = dxSlider.option("value");

        wrapper.append(valDisplay);        
        this.shadowRoot.append(wrapper);
        this.shadowRoot.appendChild(dxStyles);

    }

    displaySlideVal(slide) {
        const slideVal = slide.value;
        const slideValDisplayer = this.shadowRoot.getElementById("slideValDisplay");
        slideValDisplayer.innerText = slideVal;
    }

}