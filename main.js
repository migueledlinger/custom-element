import { YesNoItem } from "./modules/yes-no-item.js";
import { SliderItem } from "./modules/slider-item.js";
import Calender from "devextreme/ui/calendar";


customElements.define("yes-no-item", YesNoItem);
customElements.define("slider-item", SliderItem);

const dataTF = document.getElementById("dataTF");
const stateTF = document.getElementById("stateTF");

const yes_no_item = document.getElementById("item");

dataTF.addEventListener("input", () => {
    try {

        const json = JSON.parse(dataTF.value);
        const new_val = json.text;

        yes_no_item.setAttribute("data", dataTF.value);
    } catch {
        console.log("no json");
    }
});

stateTF.addEventListener("input", () => {
    try {
        const json = JSON.parse(stateTF.value);
        const new_val = json.selected;

        yes_no_item.setAttribute("state", stateTF.value);
    } catch {
        console.log("no json");
    }
})

let state = "";

const span = document.getElementById("state_inf_spn");


yes_no_item.addEventListener("stateChanged", () => showCurrentState());

// 3) shows selected answer/state in state textfield
function showCurrentState() {

    state = yes_no_item.getAttribute("state");
    stateTF.value = state;

}

const docCalDiv = document.getElementById("document-calender");
const dxCal = new Calender(docCalDiv);

